locals {
  url_map             = var.create_url_map ? join("", google_compute_url_map.default.*.self_link) : var.url_map
}

resource "google_compute_global_network_endpoint" "default-endpoint" {
  global_network_endpoint_group = google_compute_global_network_endpoint_group.neg.name
  port                          = 80
  ip_address                    = var.address
}

resource "google_compute_global_network_endpoint_group" "neg" {
  name                  = "lb-neg-group"
  default_port          = "80"
  network_endpoint_type = "INTERNET_IP_PORT"
}

resource "google_compute_global_forwarding_rule" "http" {
  project    = var.project
  name       = var.name
  target     = google_compute_target_http_proxy.default.self_link
  ip_address = google_compute_global_address.default.address
  port_range = "80"
  depends_on = [google_compute_global_address.default]
}

resource "google_compute_global_forwarding_rule" "https" {
  project    = var.project
  name       = "${var.name}-https"
  target     = google_compute_target_https_proxy.default.self_link
  ip_address = google_compute_global_address.default.address
  port_range = "443"
  depends_on = [google_compute_global_address.default]
}

resource "google_compute_global_address" "default" {
  project    = var.project
  name       = "${var.name}-address"
  ip_version = var.ip_version
}

# HTTP proxy when http forwarding is true
resource "google_compute_target_http_proxy" "default" {
  project = var.project
  name    = "${var.name}-http-proxy"
  url_map = join("", google_compute_url_map.https_redirect.*.self_link)
}

# HTTPS proxy when ssl is true
resource "google_compute_target_https_proxy" "default" {
  project = var.project
  name    = "${var.name}-https-proxy"
  url_map = local.url_map

  ssl_certificates = [google_compute_managed_ssl_certificate.default.id]
  ssl_policy       = var.ssl_policy
  depends_on = [google_compute_managed_ssl_certificate.default]
}

resource "google_compute_managed_ssl_certificate" "default" {
  provider = google-beta
  project  = var.project

  name = "${var.name}-cert"

  managed {
    domains = var.managed_ssl_certificate_domains
  }
}

resource "google_compute_url_map" "default" {
  project         = var.project
  name            = "${var.name}-url-map"
  default_service = google_compute_backend_service.default.self_link
}

resource "google_compute_url_map" "https_redirect" {
  project = var.project
  name    = "${var.name}-https-redirect"
  default_url_redirect {
    https_redirect         = true
    redirect_response_code = "MOVED_PERMANENTLY_DEFAULT"
    strip_query            = false
  }
}

resource "google_compute_backend_service" "default" {
  provider = google-beta
  project = var.project
  name    = var.name


  description                     = var.backend.description
  connection_draining_timeout_sec = var.backend.connection_draining_timeout_sec
  enable_cdn                      = var.backend.enable_cdn
  custom_request_headers          = var.backend.custom_request_headers

  # To achieve a null backend security_policy, set each.value.security_policy to "" (empty string), otherwise, it fallsback to var.security_policy.
  security_policy = var.backend.security_policy

  backend {
      description = google_compute_global_address.default.description
      group       = google_compute_global_network_endpoint_group.neg.self_link
  }

  log_config {
    enable      = var.backend.log_config.enable
    sample_rate = var.backend.log_config.sample_rate
  }
}

