terraform {
  required_version = ">= 0.12.6"
  required_providers {
    google      = ">= 3.32, <4.0.0"
    google-beta = ">= 3.32, <4.0.0"
  }
}
