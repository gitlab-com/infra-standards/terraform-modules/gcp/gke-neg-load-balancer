output "backend_services" {
  description = "The backend service resources."
  value       = google_compute_backend_service.default
}

output "external_ip" {
  description = "The external IP assigned to the global forwarding rule."
  value       = google_compute_global_address.default.address
}

output "http_proxy" {
  description = "The HTTP proxy used by this module."
  value       = google_compute_target_http_proxy.default[*].self_link
}

output "https_proxy" {
  description = "The HTTPS proxy used by this module."
  value       = google_compute_target_https_proxy.default[*].self_link
}
